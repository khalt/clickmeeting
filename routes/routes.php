<?php

use khalt\clickmeeting\ActionHandler\Payment\Create as CreatePayment;
use khalt\clickmeeting\ActionHandler\Payment\Execute;
use khalt\clickmeeting\ActionHandler\Conference\Create as CreateConference;
use Pecee\SimpleRouter\SimpleRouter;

function executeAction(string $actionHandlerClass, ?array $data = null) {
    $container = new \DI\Container();
    $actionHandler = $container->get($actionHandlerClass);
    $actionHandler->action($data ?? $_POST);
}

SimpleRouter::get('/', function () {
    require '../views/form.html';
});

SimpleRouter::post('/payment/create', function () {
    executeAction(CreatePayment::class);
});

SimpleRouter::post('/payment/execute', function () {
    executeAction(Execute::class);
});

SimpleRouter::post('/conference', function () {
    $content = trim(file_get_contents("php://input"));
    $decoded = json_decode($content, true);
    executeAction(CreateConference::class, $decoded);
});
