#filename: Vagrantfile.provision.sh
#!/usr/bin/env bash

export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8

apt-get update -qq
apt-get -y install git curl vim
apt-get install -y apache2 libapache2-mod-fastcgi apache2-mpm-worker
echo "ServerName localhost" > /etc/apache2/httpd.conf

VHOST=$(cat <<EOF
    <VirtualHost *:80>
      DocumentRoot "/var/www/public"
      ServerName app.dev
      ServerAlias app.dev
      <Directory "/var/www/public">
        AllowOverride All
        Require all granted
      </Directory>
    </VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-enabled/000-default.conf

a2enmod actions fastcgi rewrite
sudo service apache2 restart

add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y python-software-properties software-properties-common

apt-get install -y php7.1 php7.1-fpm
apt-get install -y php7.1-json php7.1-cgi php7.1-bz2 php7.1-zip
apt-get install -y libapache2-mod-php7.1
apt-get install -y php7.1-curl

a2enconf php7.1-fpm
sudo service apache2 reload

curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
cd /var/www/
composer install

echo -e "Head over to http://192.168.100.100 to get started"
