paypal.Button.render({
    env: 'sandbox',
    commit: true,
    payment: () => {
        let CREATE_URL = '/payment/create';
        return paypal.request.post(CREATE_URL)
            .then(res => res.data.id);
    },
    onAuthorize: (data, actions) => {
        let EXECUTE_URL = '/payment/execute',
            form = document.getElementById('form'),
            info = {};

        for (i = 0; i < form.length; i++) {
            info[form.elements[i].name] = form.elements[i].value;
        }
        info.paymentId = data.paymentID;
        data = {
            paymentID: data.paymentID,
            payerID: data.payerID
        };
        return paypal.request.post(EXECUTE_URL, data)
            .then(res => {
                document.getElementById('form').style = 'display: none;';
                document.getElementById('paypal-button-container').style = 'display: none;';
                document.getElementById('success').style = 'display: block;';
                postData('/conference', info)
                    .then(data =>{
                        let button = document.getElementById('submit');
                        button.addEventListener('click', () => {
                            window.location.replace(data.data.roomUrl);
                        });
                        document.getElementById('submit').style = 'display: block;';

                    })
                    .catch(error => console.error(error))
            });
    }
}, '#paypal-button-container');


function postData(url, data) {
    return fetch(url, {
        body: JSON.stringify(data),
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'user-agent': 'Mozilla/4.0 MDN Example',
            'content-type': 'application/json'
        },
        method: 'POST',
        mode: 'cors',
        redirect: 'follow',
        referrer: 'no-referrer',
    })
        .then(response => response.json())
}
