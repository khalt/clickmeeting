<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../routes/routes.php';

use Pecee\SimpleRouter\SimpleRouter;

$dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
$dotenv->load();

SimpleRouter::start();
