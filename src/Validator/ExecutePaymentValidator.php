<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Validator;

use khalt\clickmeeting\Validator\Type\StringType;

class ExecutePaymentValidator extends AbstractValidator
{
    protected $fields = [
        'paymentID' => StringType::class,
        'payerID' => StringType::class,
    ];
}