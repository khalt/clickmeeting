<?php
declare(strict_types = 1);

namespace khalt\clickmeeting\Validator\Type;

interface TypeInterface
{
    public function isOfType($param): bool;
    public function castToType($param);
    public function toString(): string;
}
