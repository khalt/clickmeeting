<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Validator\Type;

class StringType implements TypeInterface
{
    public function isOfType($param): bool
    {
        return is_string($param);
    }

    public function castToType($param)
    {
        return (string)$param;
    }

    public function toString(): string
    {
        return 'string';
    }
}