<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Validator\Type;

class IntType implements TypeInterface
{

    public function isOfType($param): bool
    {
        return is_numeric($param);
    }

    public function castToType($param)
    {
        return (int)$param;
    }

    public function toString(): string
    {
        return 'int';
    }
}