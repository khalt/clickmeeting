<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Validator;

use khalt\clickmeeting\Validator\Type\StringType;

class CreateConferenceValidator extends AbstractValidator
{
    protected $fields = [
        'paymentId' => StringType::class,
        'email' => StringType::class,
        'nickname' => StringType::class,
    ];
}
