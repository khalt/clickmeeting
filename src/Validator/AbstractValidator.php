<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Validator;

use khalt\clickmeeting\Exception\MissingInputException;
use khalt\clickmeeting\Exception\WrongInputTypeException;
use khalt\clickmeeting\Validator\Type\TypeInterface;

abstract class AbstractValidator
{
    protected $fields;
    protected $data;

    /**
     * AbstractValidator constructor.
     * @param array $data
     * @throws WrongInputTypeException
     * @throws MissingInputException
     */
    public function __construct(array $data)
    {
        $this->data = $this->validate($data);
    }

    /**
     * @param array $data
     * @return array
     * @throws WrongInputTypeException
     * @throws MissingInputException
     */
    private function validate(array $data): array
    {
        $difference = array_diff(array_keys($this->fields), array_keys($data));
        if (count($difference) > 0) {
            throw new MissingInputException($difference);
        }
        return array_combine(array_keys($data), array_map(function ($key, $item) {
            /** @var TypeInterface $itemType */
            $itemType = new $this->fields[$key]();
            if (!$itemType->isOfType($item)) {
                throw new WrongInputTypeException($key, $itemType->toString(), $item);
            }
            return $itemType->castToType($item);
        }, array_keys($data), array_values($data)));
    }

    public function getData(): array
    {
        return $this->data;
    }
}