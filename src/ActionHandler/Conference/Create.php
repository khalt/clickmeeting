<?php
declare(strict_types=1);

namespace khalt\clickmeeting\ActionHandler\Conference;

use khalt\clickmeeting\ActionHandler\ActionHandlerInterface;
use khalt\clickmeeting\Exception\ClickMeetingException;
use khalt\clickmeeting\Exception\MissingInputException;
use khalt\clickmeeting\Exception\WrongInputTypeException;
use khalt\clickmeeting\Response\JsonResponse;
use khalt\clickmeeting\Service\ClickMeetingService;
use khalt\clickmeeting\Service\PaymentService;
use khalt\clickmeeting\Validator\CreateConferenceValidator;

class Create implements ActionHandlerInterface
{
    private $paymentService;
    private $clickMeetingService;

    public function __construct(PaymentService $paymentService, ClickMeetingService $clickMeetingService)
    {
        $this->paymentService = $paymentService;
        $this->clickMeetingService = $clickMeetingService;
    }

    public function action(array $requestData = []): JsonResponse
    {
        try {
            $conferenceData = (new CreateConferenceValidator($requestData))->getData();
            $payment = $this->paymentService->getPayment($conferenceData['paymentId']);
            $roomUrl = $this->clickMeetingService->createConference($payment, $conferenceData);
        } catch (MissingInputException | WrongInputTypeException $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::UNPROCESSABLE);
        } catch (ClickMeetingException $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::UNAVAILABLE);
        }

        return new JsonResponse(['roomUrl' => $roomUrl]);
    }
}
