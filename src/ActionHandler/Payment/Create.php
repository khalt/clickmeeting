<?php
declare(strict_types=1);

namespace khalt\clickmeeting\ActionHandler\Payment;

use khalt\clickmeeting\ActionHandler\ActionHandlerInterface;
use khalt\clickmeeting\Exception\MissingInputException;
use khalt\clickmeeting\Exception\PayPalException;
use khalt\clickmeeting\Exception\WrongInputTypeException;
use khalt\clickmeeting\Response\JsonResponse;
use khalt\clickmeeting\Response\PaymentResponse;
use khalt\clickmeeting\Service\PaymentService;

class Create implements ActionHandlerInterface
{
    private $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function action(array $requestData = []): JsonResponse
    {
        try {
            $payment = $this->paymentService->createPayment();
        } catch (PayPalException $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::UNAVAILABLE);
        } catch (WrongInputTypeException | MissingInputException $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::UNPROCESSABLE);
        }

        return new JsonResponse(new PaymentResponse($payment));
    }
}
