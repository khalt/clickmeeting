<?php
declare(strict_types=1);

namespace khalt\clickmeeting\ActionHandler;

interface ActionHandlerInterface
{
    public function action(array $requestData = []);
}