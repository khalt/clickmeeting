<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Response;

class JsonResponse
{
    const OK = 200;
    const NO_CONTENT = 204;
    const CREATED = 201;
    const NOT_FOUND = 404;
    const UNPROCESSABLE = 422;
    const UNAVAILABLE = 503;

    const STATUSES = [
        self::OK => 'OK',
        self::NO_CONTENT => 'No content',
        self::CREATED => 'Created',
        self::NOT_FOUND => 'Not found',
        self::UNPROCESSABLE => 'Unprocessable entity',
        self::UNAVAILABLE => 'Service Unavailable',
    ];

    private $data;
    private $statusCode;
    private $status;
    private $headers;

    public function __construct($data = null, int $statusCode = self::OK, array $headers = [])
    {
        $this->data = $data;
        $this->headers = $headers;
        $this->statusCode = $statusCode;
        $this->status = self::STATUSES[$statusCode];
        echo $this->respond();
    }

    public function respond(): string
    {
        header("Content-Type: application/json");
        header(sprintf('HTTP/1.1 %s %s', $this->statusCode, $this->status), true, $this->statusCode);

        if (count($this->headers) > 0) {
            foreach ($this->headers as $headerName => $value) {
                header("{$headerName}: {$value}");
            }
        }
        $result = [];
        if (count($this->data) > 0) {
            $result['data'] = $this->data;
        }

        return json_encode($result);
    }
}
