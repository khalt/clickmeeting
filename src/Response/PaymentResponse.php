<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Response;

use PayPal\Api\Payment;

class PaymentResponse implements \JsonSerializable
{
    private $data;

    public function __construct(Payment $payment)
    {
        $this->data = $this->prepare($payment);
    }

    private function prepare(Payment $payment)
    {
        return $payment->toArray();
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}