<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Service;

use khalt\clickmeeting\Exception\MissingInputException;
use khalt\clickmeeting\Exception\PayPalException;
use khalt\clickmeeting\Exception\WrongInputTypeException;
use khalt\clickmeeting\Validator\ExecutePaymentValidator;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

class PaymentService
{
    private $apiContext;

    public function __construct()
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                getenv('PAYPAL_CLIENT_ID'),
                getenv('PAYPAL_SECRET')
            )
        );
    }

    public function getPayment(string $paymentId): Payment
    {
        return Payment::get($paymentId, $this->apiContext);
    }

    /**
     * @return Payment
     * @throws MissingInputException
     * @throws PayPalException
     * @throws WrongInputTypeException
     */
    public function createPayment(): Payment
    {
        $payer = new Payer();
        $amount = new Amount();
        $transaction = new Transaction();
        $redirectUrls = new RedirectUrls();
        $payment = new Payment();
        $payer->setPaymentMethod('paypal');
        $amount->setTotal(getenv('ROOM_COST'));
        $amount->setCurrency('PLN');
        $transaction->setAmount($amount);
        $redirectUrls->setReturnUrl(getenv('REDIRECT_OK'))->setCancelUrl(getenv('REDIRECT_OK'));
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);
        try {
            $payment->create($this->apiContext);
        } catch (PayPalConnectionException $ex) {
            throw new PayPalException($ex->getData());
        }
        return $payment;
    }

    /**
     * @param array $paymentData
     * @return Payment
     * @throws PayPalException
     * @throws WrongInputTypeException
     * @throws MissingInputException
     */
    public function executePaymentById(array $paymentData): Payment
    {
        $paymentData = (new ExecutePaymentValidator($paymentData))->getData();
        $payment = $this->getPayment($paymentData['paymentID']);
        $execution = new PaymentExecution();
        try {
            $execution->setPayerId($paymentData['payerID']);
            $payment->execute($execution, $this->apiContext);
        } catch (\Exception $e) {
            throw new PayPalException($e->getMessage());
        }
        return $this->getPayment($paymentData['paymentID']);
    }
}
