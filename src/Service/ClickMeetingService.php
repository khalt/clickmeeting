<?php
declare(strict_types = 1);

namespace khalt\clickmeeting\Service;

use khalt\clickmeeting\ClickMeetingRestClient\ClickMeetingRestClient;
use khalt\clickmeeting\Exception\ClickMeetingException;
use khalt\clickmeeting\Exception\PaymentNotFulfilledException;
use PayPal\Api\Payment;

class ClickMeetingService
{
    private $clickMeetingClient;

    public function __construct()
    {
        $this->clickMeetingClient = new ClickMeetingRestClient(
            [
                'api_key' => getenv('CLICKMEETING_API_KEY'),
                'format' => 'json',
            ]
        );
    }

    /**
     * @param Payment $payment
     *
     * @return string
     * @throws ClickMeetingException
     */
    public function createConference(Payment $payment, array $data): string
    {
        try {
            if ($payment->getState() !== 'approved') {
                throw new PaymentNotFulfilledException();
            }
            $randRoomId = rand(0, 100);
            $room = json_decode(
                $this->clickMeetingClient->addConference(
                    [
                        'name' => "{$data['nickname']}_room_{$randRoomId}",
                        'room_type' => 'meeting',
                        'permanent_room' => 0,
                        'access_type' => 1,
                    ]
                )
            );
            $hash = json_decode(
                $this->clickMeetingClient->conferenceAutologinHash(
                    $room->room->id,
                    [
                        'email' => "{$data['email']}",
                        'nickname' => "{$data['nickname']}",
                        'role' => 'listener',
                    ]
                )
            );
        } catch (\Exception $e) {
            throw new ClickMeetingException($e->getMessage());
        }

        return "{$room->room->room_url}?l={$hash->autologin_hash}";
    }
}
