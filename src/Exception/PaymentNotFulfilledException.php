<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Exception;

class PaymentNotFulfilledException extends PayPalException
{
    public function __construct()
    {
        parent::__construct('Payment is not fulfilled.');
    }
}
