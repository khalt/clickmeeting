<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Exception;

class WrongInputTypeException extends \Exception
{
    public function __construct(string $field, string $expectedType, $value)
    {
        parent::__construct("{$field} must be of {$expectedType} type. Current value is: {$value}.");
    }
}