<?php
declare(strict_types=1);

namespace khalt\clickmeeting\Exception;

class MissingInputException extends \Exception
{
    public function __construct(array $fields)
    {
        parent::__construct("Missing required arguments: " . implode(", ", $fields));
    }
}